#vaadin-upload-firebase

[vaadin-upload](https://vaadin.com/elements/-/element/vaadin-upload) is a  [Polymer](http://polymer-project.org) element for uploading multiple files, part of the [vaadin-core-elements](https://vaadin.com/elements) element bundle.

[vaadin-upload-firebase] Add the support to Firebase javascript SDK.

```html
<vaadin-upload accept=".pdf" [user]="user" ["firebaseRef"]="firebaseRef" ["firebaseRefDB"]="firebaseRef">
  <div class="drop-label">
    <iron-icon icon="description"></iron-icon>
    Drop your favourite Novels here (PDF files only)
  </div>
</vaadin-upload>
```

[<img src="https://raw.githubusercontent.com/vaadin/vaadin-upload/master/docs/img/vaadin-upload-overview.png" alt="Screenshot of vaadin-upload" width="723" />](https://vaadin.com/elements/-/element/vaadin-upload)


## License

Apache License 2.0